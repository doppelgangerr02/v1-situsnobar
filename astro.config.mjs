import { defineConfig } from "astro/config";
import tailwind from "@astrojs/tailwind";
import image from "@astrojs/image";
import sitemap from "@astrojs/sitemap";
import Critters from "astro-critters";
import siteData from "./src/siteConfig";
import vue from '@astrojs/vue';

// https://astro.build/config
export default defineConfig({
  site: siteData.siteUrl,

  integrations: [
    vue({
      template:{
        compilerOptions:{
          isCustomElement:(tag)=>{
            return tag.includes('amp')
          }
        }
      }
    }),
    Critters(),
    image({
      serviceEntryPoint: "@astrojs/image/sharp",
    }),

    // tailwind({
    //   config: {
    //     applyBaseStyles: false,
    //   },
    // }),
    sitemap(),
  ],
});
