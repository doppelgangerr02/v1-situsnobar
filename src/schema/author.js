import siteData from "../siteConfig"
const authorSchema = {
  "@context": "https://schema.org",
  "@type": "Game",
  "name": siteData.author,
  "author": {
    "@type": "Person",
    "name": siteData.author
  },
  "image": siteData.siteUrl + `/imgs/situsnobar.png`,
  "url": siteData.siteUrl,
  "publisher": {
    "@type": "Organization",
    "name": siteData.author
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "99",
    "bestRating": "100",
    "worstRating": "2",
    "ratingCount": "35565419"
  },
  "inLanguage": "id"
}

export default { authorSchema }