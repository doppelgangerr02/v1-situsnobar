
import siteData from "../siteConfig"
const BreadcrumbList = {
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
    {
      "@type": "ListItem",
      "position": 1,
      "name": siteData.title,
      "item": {
        "@type": "WebSite",
        "@id": siteData.siteUrl
      },
      "name": siteData.author
    }

  ]
}


export default { BreadcrumbList }
