import siteData from "../siteConfig";
const faq = {
  "@context": "https://schema.org/",
  "@type": "FAQPage",
  "name": "FAQ Pertanyaan Populer Seputar Situs Daftar Situs Nobar Paling Lengkap",
  "mainEntity": [
    {
      "@type": "Question",
      "name": `Bagaimana cara menonton di ${siteData.siteName}?`,
      "acceptedAnswer": {
        "@type": "Answer",
        "text": `Untuk menonton pertandingan yang kamu inginkan, kalian hanya perlu memilih salah satu dari beberapa situs yang sudah kami sediakan dan kami tampilkan di halaman depan situs ${siteData.siteName} `
      }
    },
    {
      "@type": "Question",
      "name": `Apakah dikenakan biaya untuk menonton di ${siteData.siteName}?`,
      "acceptedAnswer": {
        "@type": "Answer",
        "text": `${siteData.siteName} tidak memungut biaya sepeserpun bagi kalian yang menggunakan layanan kami, artinya kalian dapat menonton siaran sepakbola dan basket dunia secara gratis.`
      }
    },
    {
      "@type": "Question",
      "name": `Apakah  daftar situs ${siteData.siteName} selalu di update?`,
      "acceptedAnswer": {
        "@type": "Answer",
        "text": `Ya, semua situs yang kami tampilkan sudah kami pilah dan pilih dan semua situs tersebut bisa kalian akses 24 jam sehari.`
      }
    }
  ]
}

export default { faq }