import siteData from "../siteConfig";

const newsArticle =

{
  "@context": "https://schema.org/",
  "@type": "NewsArticle",
  "author": {
    "@type": "Organization",
    "url": siteData.siteUrl,
    "name": siteData.author
  },
  "headline": siteData.title,
  "image": {
    "@type": "ImageObject",
    "url": {}
  },
  "publisher": {
    "@type": "Organization",
    "name": siteData.author,
    "logo": {
      "@type": "ImageObject",
      "url": siteData.siteUrl + `/imgs/situsnobar.png`
    },
    "address": "Indonesia",
    "location": "Indonesia",
    "identifier": siteData.siteUrl
  },
  "keywords": siteData.keywords.split(','),
  "datePublished": "2023-07-17T12:03:59+00:00",
  "dateModified": "2023-07-17T12:03:59+00:00",
  "mainEntityOfPage": siteData.siteUrl,
  "description": siteData.description,
  "speakable": siteData.siteUrl
}

export default { newsArticle }